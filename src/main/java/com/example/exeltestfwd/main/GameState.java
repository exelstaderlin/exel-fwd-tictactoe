package com.example.exeltestfwd.main;

import com.example.exeltestfwd.model.GameModel;
import com.example.exeltestfwd.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameState {

    public GameModel gameModel;
	public Board board;
	private static final Logger log = LoggerFactory.getLogger(GameState.class);

    public GameState() {
		gameModel = new GameModel();
		board = new Board();
		clearData();
	}
	
	public void startNewGame() {
        gameModel.gameMessage = "";
        gameModel.turn = Marker.X;
        gameModel.turnMessage = "Turn: X";
		gameModel.gameStage = GameStage.IN_GAME;

		board = new Board(gameModel.gameSize);
		board.clear();
	}
	
	public void reset() {
        clearData();
        board.clear();
	}

	public void setModeSelection(String mode) {
		gameModel.xPlayerName = "";
		if(mode.equals("ai")) {
			gameModel.gameMode = GameMode.AI_VS_HUMAN;
		}
		else if(mode.equals("twoplayer")) {
			gameModel.gameMode = GameMode.HUMAN_VS_HUMAN;
		}
		else {
			throw new RuntimeException("Invalid selected game mode:" + mode);
		}
	}

	public void setGameSize(int size) {
		gameModel.gameSize = size;
	}

	public String getContentView() {
		if(gameModel.gameStage == GameStage.MODE_SELECTION) 
			return Constants.VIEW_MAIN_MENU;
		else 	
			return Constants.VIEW_GAME;
	}

	private void clearData() {
		gameModel.xPlayerName = "X Player";
        gameModel.oPlayerName = "O Player";
        gameModel.gameMessage = "";
        gameModel.turn = Marker.X;
        gameModel.turnMessage = "Turn: X";
        gameModel.gameMode = GameMode.AI_VS_HUMAN;
		gameModel.gameStage = GameStage.MODE_SELECTION;
		gameModel.gameSize = 0;
	}
	
	public void gameMove(Integer row, Integer col) {
		try {
			if(gameModel.gameStage.equals(GameStage.IN_GAME)) {
				board.move(row, col, gameModel.turn);
				evaluateBoard(); //refresh the board

				//if the opponent is bot, the bot will take the move
				if(gameModel.gameStage.equals(GameStage.IN_GAME) 
				&& gameModel.gameMode.equals(GameMode.AI_VS_HUMAN))
					botTakeMoves();	
			}
		}
		catch( Exception e ){
			log.error("Cannot complete move", e);
		}
	}

	private void botTakeMoves() {
		AiBot aiBot = new AiBot(gameModel, board);
		aiBot.determineBestMove(); //bot calculate moves
		evaluateBoard(); //refresh the board
	}


	private void evaluateBoard() {
		if(board.isDraw()) {
			gameModel.gameMessage = "It's a draw!";
			gameModel.gameStage = GameStage.POST_GAME;
		}
		else if(board.isWinner(gameModel.turn)) {
			if(gameModel.turn.equals(Marker.O)) 
				gameModel.gameMessage = "O wins!";
			else 
				gameModel.gameMessage = "X wins!";

			gameModel.gameStage = GameStage.POST_GAME;
		}
		else
		{
			if(gameModel.turn == Marker.X) {
				gameModel.turn = Marker.O;
				gameModel.turnMessage = "Turn: O";
			}
			else {
				gameModel.turn = Marker.X;
				gameModel.turnMessage = "Turn: X";
			}
		}
	}
	
	public enum GameMode {
		AI_VS_HUMAN, 
		HUMAN_VS_HUMAN
	};
	
	public enum GameStage {
		MODE_SELECTION, 
		PLAYER_IDENTIFICATION,
		IN_GAME,
		POST_GAME
	}

	public enum Marker { 
		BLANK,
		X,
		O
	 };
}