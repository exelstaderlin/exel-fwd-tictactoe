package com.example.exeltestfwd.main;

import java.util.Random;

import com.example.exeltestfwd.main.GameState.Marker;
import com.example.exeltestfwd.model.GameModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AiBot {

    GameModel gameModel;
    Board board;
    int gameSize;
    Marker boardMarker[][];
    Marker playerMarker;
    Marker opponentMarker;
    int centerRow;
	private static final Logger log = LoggerFactory.getLogger(GameState.class);

    public AiBot(GameModel model, Board bd) {
        gameModel = model;
        board = bd;

        gameSize = gameModel.gameSize;
		boardMarker = board._marker;
		playerMarker = gameModel.turn;
		opponentMarker = playerMarker.equals(Marker.X) ? Marker.O : Marker.X ;
		centerRow = (board._marker.length - 1) / 2;  //centerRow example: 3x3 so (3-1)/2 = 1, 7x7(7-1)/2=3
    }
    
    public void determineBestMove() {
		
        if(checkAvailableCenterBox())
            return;
        if(checkVerticalMove())
            return;
		if(checkHorizontalMove())
            return;
        if(checkForLeftDiagonal())
            return;
        if(checkForRightDiagonal())
            return;

		getRandomPosition();
    }
    
    private boolean checkAvailableCenterBox() {
        //bot always take the center box if available
		if( boardMarker[centerRow][centerRow].equals(Marker.BLANK)) { //if center box is empty
			try {
				board.move(centerRow, centerRow, playerMarker);
				return true;
			}
			catch(Exception e) {
				log.info(e.toString());
				
			}
        }
        return false;
    }

    private boolean checkVerticalMove() {
        // Next, check if there is a block move in the verticals.
		for(int r = 0; r < gameSize; ++r) {
			int bCount = 0;
			int oCount = 0;
			for(int c = 0; c < gameSize; ++c) {
				if(boardMarker[r][c].equals(opponentMarker)) {
					++oCount;
				}
				if(boardMarker[r][c].equals(Marker.BLANK)) {
					++bCount;
				}
			}
			
			// If there were two opponent markers and a blank,
			// move to the blank spot.
			if((oCount == 2) && (bCount == gameSize - 2)) {
				for(int c = 0; c < gameSize; ++c) {
					if(boardMarker[r][c].equals(Marker.BLANK)) {
						try {
							board.move(r, c, playerMarker);
							return true;
						}
						catch(Exception e) {
							
						}
					}
				}
			}
		}
        return false;
    }

    private boolean checkHorizontalMove() {
        // Next, check rows for blockers.
		for(int c = 0; c < gameSize; ++c) {
			int bCount = 0;
			int oCount = 0;
			for(int r = 0; r < gameSize; ++r) {
				if(boardMarker[r][c].equals(opponentMarker)) {
					++oCount;
				}
				if(boardMarker[r][c].equals(Marker.BLANK)) {
					++bCount;
				}
			}
			
			// If there were two opponent markers and a blank,
			// move to the blank spot.
			if((oCount == 2) && (bCount == gameSize-2)) {
				for(int r = 0; r < gameSize; ++r) {
					if(boardMarker[r][c].equals(Marker.BLANK)) {
						try {
							board.move(r, c, playerMarker);
							return true;
						}
						catch(Exception e) {
							
						}
					}
				}
			}
		}
        return false;
    }

    private boolean checkForLeftDiagonal() {
        //check for diagonals left
		int bCount = 0;
		int oCount = 0;
		int r = 0;
		int c = 0;
		for(int i = 0; i < gameSize; ++i) {
			if(boardMarker[r][c].equals(opponentMarker)) {
				++oCount;
			}
			if(boardMarker[r][c].equals(Marker.BLANK)) {
				++bCount;
			}
			++r;
			++c;
		}
		if((oCount == 2) && (bCount == gameSize-2)) {
			r = 0;
			c = 0;
			for(int i = 0; i < gameSize; ++i) {
				if(boardMarker[r][c].equals(Marker.BLANK)) {
					try {
						board.move(r, c, playerMarker);
						return true;
					}
					catch(Exception e) {
						
					}
				}
				++r;
				++c;
			}
		}
        return false;
    }

    private boolean checkForRightDiagonal() {
        //check for diagonas right
        int r = 0;
		int c = gameSize-1;
		int bCount = 0;
		int oCount = 0;
		for(int i = 0; i < gameSize; ++i) {
			if(boardMarker[r][c].equals(opponentMarker)) {
				++oCount;
			}
			if(boardMarker[r][c].equals(Marker.BLANK)) {
				++bCount;
			}
			++r;
			--c;
		}
		if((oCount == 2) && (bCount == gameSize-2)) {
			r = 0;
			c = gameSize-1;
			for(int i = 0; i < gameSize; ++i) {
				if(boardMarker[r][c].equals(Marker.BLANK)) {
					try {
						board.move(r, c, playerMarker);
						return true;
					}
					catch(Exception e) {
						
					}
				}
				++r;
				--c;
			}
		}
        return false;
    }

    private void getRandomPosition() {
        // Keep generating random positions until a blank spot is found
		boolean found = false;
		Random random = new Random();
		while(!found) {
			int r = random.nextInt(gameSize);
			int  c = random.nextInt(gameSize);
			if(boardMarker[r][c].equals(Marker.BLANK)) {
				try {
					board.move(r, c, playerMarker );
					found = true;
				}
				catch(Exception e) {
					log.error("Problem making random move!", e);
				}			
			}
		}
    }

}