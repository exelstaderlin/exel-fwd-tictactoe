package com.example.exeltestfwd.main;

import java.util.ArrayList;
import java.util.List;

import com.example.exeltestfwd.main.GameState.Marker;


public class Board {

	private int gameSize;
	public Marker[][] _marker;
	
	public Board() {
	}

	public Board(int size) {
		gameSize = size	;
		_marker = new Marker[size][size];
	}

	/**
	 * Clears the TicTacTow board of all of the markers.
	 */
	public void clear() {
		for(int r = 0;  r < gameSize;  ++r ) {
			for(int c = 0;  c < gameSize;  ++c) {
				_marker[r][c] = Marker.BLANK;
			}
		}
	}
	
	public String markAt(int row, int col)
	{
		Marker marker = _marker[row][col];
		if(marker.equals(Marker.X)) {
			return "X";
		}
		else if(marker.equals(Marker.O)) {
			return "O";
		}
		else if(marker.equals(Marker.BLANK)) {
			return " ";
		}
		return "#";
	}

	public List<List<int[]>> generateGameBoard() { //[[[0,0],[0,1],[0,2]],[[1,0],[1,1],[1,2]],[[2,0],[2,1],[2,2]]]
		List<List<int[]>> grid = new ArrayList<>();
		for(int i=0; i<gameSize; i++) {
			List<int[]> row = new ArrayList<>();
			for(int j=0; j<gameSize; j++) {
				int[] arrInt = {i,j};
				row.add(arrInt);
			}
			grid.add(row);
		}
		return grid;
	}

	public void move(int row, int col, Marker marker) throws Exception {
		if( _marker[row][col] != Marker.BLANK) {
			throw new Exception( "Square @ (" + row + ", " + col + ") is not empty");
		}
		if(marker == Marker.BLANK) {
			throw new IllegalArgumentException("Playing a BLANK marker is not valid");
		}
		_marker[row][col] = marker;
	}

	public boolean isWinner(Marker marker) {
		
		if(checkVerticalWinner(marker)) 
			return true;

		if(checkHorizontalWinner(marker)) 
			return true;
		
		if(checkDiagonalLeftWinner(marker))
			return true;
		
		if(checkDiagonalRightWinner(marker))
			return true;
		
		return false;
	}

	private boolean checkHorizontalWinner(Marker marker) {
		for(int c = 0; c < gameSize;  ++c) {
			boolean isWinner = true;
			int fourWinCondition = 0;
			for(int r = 0; r < gameSize; ++r) {
				if(_marker[r][c] != marker) {
					isWinner = false;
					fourWinCondition = 0;
				}
				else 
					fourWinCondition++;
					
				if(fourWinCondition == 4) { // for 5x5.7x7.9x9
					return true;
				}
			}
			if(isWinner) {
				return true;
			}
		}
		return false;
	}

	private boolean checkVerticalWinner(Marker marker) {
		for(int r = 0; r < gameSize;  ++r) {
			boolean isWinner = true;
			int fourWinCondition = 0;
			for(int c = 0; c < gameSize; ++c) {
				if(_marker[r][c] != marker) {
					isWinner = false;
					fourWinCondition = 0;
				}
				else 
					fourWinCondition++;
			
				if(fourWinCondition == 4) // for 5x5.7x7.9x9
					return true;
			}
			if(isWinner) {
				return true;
			}
		}
		return false;
	}

	private boolean checkDiagonalLeftWinner(Marker marker) {
		boolean isWinner = true;
		int fourWinCondition = 0;
		for(int r = 0; r < gameSize;  ++r) {
			if(_marker[r][r] != marker) {
				isWinner = false;
				fourWinCondition = 0;
			}
			else 
				fourWinCondition++;
			
			if(fourWinCondition == 4) // for 5x5.7x7.9x9
				return true;
		}
		if(isWinner) {
			return true;
		}
		return false;
	}

	private boolean checkDiagonalRightWinner(Marker marker) {
		boolean isWinner = true;
		int fourWinCondition = 0;
		for(int r = 0; r < gameSize;  ++r) {
			if(_marker[r][(gameSize-1)-r] != marker) {
				isWinner = false;
				fourWinCondition = 0;
			}	
			else 
				fourWinCondition++;
			
			if(fourWinCondition == 4) // for 5x5.7x7.9x9
				return true;
		}
		if(isWinner) {
			return true;
		}
		return false;
	}

	public boolean isDraw() {
		// If all squares are filled, and a winner not declared
		for(int r = 0 ;  r < gameSize;  ++r) {
			for(int c = 0 ;  c < gameSize;  ++c) {
				if(_marker[r][c].equals(Marker.BLANK)) {
					return false;
				}
			}
		}
		return true;
	}
}
