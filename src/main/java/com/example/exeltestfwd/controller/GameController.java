package com.example.exeltestfwd.controller;

import javax.servlet.http.HttpSession;

import com.example.exeltestfwd.main.GameState;
import com.example.exeltestfwd.util.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GameController {	
		
	private static final Logger log = LoggerFactory.getLogger(GameController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String mainMenu(HttpSession session,Model model) {
		GameState gameState = getStateFromSession(session);
		model.addAttribute(Constants.GAME_STATE, gameState);
		return gameState.getContentView();
	}
	
	@RequestMapping(value = "/tictactoe/reset", method = RequestMethod.GET)
	public String reset(HttpSession session,Model model) {
		log.info("Resetting new game");
		GameState gameState = getStateFromSession(session);
		putStateInSession(session, gameState);
		gameState.reset();
		model.addAttribute(Constants.GAME_STATE, gameState);

		return gameState.getContentView();
	}

	
	@RequestMapping(value = "/tictactoe/selectSize", method = RequestMethod.GET)
	public String gameSize(
			HttpSession session,
			Model model
	) {
		log.info("Select Game Size");
		GameState gameState = getStateFromSession(session);
		model.addAttribute(Constants.GAME_STATE, gameState);
		return Constants.VIEW_GAMESIZE_MENU;
	}

	@RequestMapping(value = "/tictactoe/modeselection", method = RequestMethod.GET)
	public String modeSelected(
			HttpSession session,
			@RequestParam(value = "mode", required = true) String mode,
			Model model
	) {
		
		GameState gameState = getStateFromSession(session);
		model.addAttribute(Constants.GAME_STATE, gameState);
		gameState.setModeSelection(mode);
		return "redirect:/tictactoe/selectSize";
	}

	@RequestMapping(value = "/tictactoe/modegamesize", method = RequestMethod.GET)
	public String modeGameBoardSize(
			HttpSession session,
			@RequestParam(value = "size", required = true) int size,
			Model model
	) {
		
		GameState gameState = getStateFromSession(session);
		gameState.setGameSize(size);
		model.addAttribute(Constants.GAME_STATE, gameState);
		return "redirect:/tictactoe/new";
	}

		
	@RequestMapping(value = "/tictactoe/new", method = RequestMethod.GET)
	public String gameNew(HttpSession session,Model model) {
		log.info("Starting new game");
		GameState gameState = getStateFromSession(session);
		model.addAttribute(Constants.GAME_STATE, gameState);
		gameState.startNewGame();
		return gameState.getContentView();
	}
	
	@RequestMapping(value = "/tictactoe/move", method = RequestMethod.GET)
	public String playerMove(
			HttpSession session,
			@RequestParam(value = "row", required = true) Integer row, 
			@RequestParam(value = "col", required = true) Integer col, 
			Model model) {
				
		log.info("move=(" + row + ", " + col + ")");
		GameState gameState = getStateFromSession(session);
		model.addAttribute(Constants.GAME_STATE, gameState);
		gameState.gameMove(row, col);
		return gameState.getContentView();
	}

	private GameState getStateFromSession(HttpSession session)
	{
		GameState gameState = (GameState)session.getAttribute(Constants.GAME_STATE);
		if(gameState == null) {
			gameState = new GameState();
			putStateInSession(session, gameState);
			log.info("New GameState created and put in session");
		}
		return gameState;
	}
	

	private void putStateInSession(HttpSession session, GameState gameState) {
		session.setAttribute(Constants.GAME_STATE, gameState);
	}
}
