package com.example.exeltestfwd.util;

public class Constants {
	public static final String GAME_STATE = "gameState";
	
	public static final String VIEW_START = "start";
	public static final String VIEW_PLAYER_SELECT = "playerselect";
	public static final String VIEW_GAME = "tictactoe";
	public static final String VIEW_MAIN_MENU = "main_menu";
	public static final String VIEW_GAMESIZE_MENU = "menu_game_size";
}
