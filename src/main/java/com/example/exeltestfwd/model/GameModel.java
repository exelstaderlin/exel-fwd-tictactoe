package com.example.exeltestfwd.model;

import com.example.exeltestfwd.main.Board;
import com.example.exeltestfwd.main.GameState.GameMode;
import com.example.exeltestfwd.main.GameState.GameStage;
import com.example.exeltestfwd.main.GameState.Marker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameModel {

	public String xPlayerName;
	public String oPlayerName;
	public String gameMessage;
	public String turnMessage;
	public Marker turn;
	public GameMode gameMode;
	public GameStage gameStage;
	public int gameSize;

	private static final Logger log = LoggerFactory.getLogger(GameModel.class);
	
	@Override
	public String toString() {
		return "GameState [xPlayerName=" + xPlayerName + ", oPlayerName=" + oPlayerName + ", gameMessage=" + gameMessage
				+ ", turnMessage=" + turnMessage + ", turn=" + turn + ", gameMode=" + gameMode + ", gameStage="
				+ gameStage + "]";
	}

		
}
